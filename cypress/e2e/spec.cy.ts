describe('visit test', () => {
  it('test visit http://localhost:4200/add', () => {
    cy.visit('http://localhost:4200/add')
  })

  it('test visit http://localhost:4200/tutorials', () => {
    cy.visit('http://localhost:4200/tutorials')
  })

})

describe('test add', () => {

  it('test visit http://localhost:4200/add', () => {
    cy.visit('http://localhost:4200/add')
  })

  it('check empty input', () => {
    cy.get('#title').should('have.value', '');
  })

  it('input title', () => {
    cy.get('#title').type('ice');
    cy.get('#title').should('have.value', 'ice');
  })

  it('input description', () => {
    cy.get('#description').type('good vendor');
    cy.get('#description').should('have.value', 'good vendor');
  })

  it('click button', () => {
    cy.get('#btn').click()
  })

  it('test add success', () => {
    cy.get('#success-text').should('have.text', 'Submitted successfully!');
  })

  it('visit http://localhost:4200/tutorials', () => {
    cy.visit('http://localhost:4200/tutorials')
  })

  it('check have less 1 item', () => {
    cy.get('#index0')
  })

})

describe('test remove all', () => {

  it('visit http://localhost:4200/tutorials', () => {
    cy.visit('http://localhost:4200/tutorials')
  })

  it('click button', () => {
    cy.get('#remove-btn').click()
  })

})

describe('test add multiple items', () => {

  it('test visit http://localhost:4200/add', () => {
    cy.visit('http://localhost:4200/add')
  })

  it('input title', () => {
    cy.get('#title').type('title1');
    cy.get('#title').should('have.value', 'title1');
  })

  it('input description', () => {
    cy.get('#description').type('description1');
    cy.get('#description').should('have.value', 'description1');
  })

  it('click button', () => {
    cy.get('#btn').click()
  })

  it('click button', () => {
    cy.get('#add-btn').click()
  })

  it('input title', () => {
    cy.get('#title').type('title2');
    cy.get('#title').should('have.value', 'title2');
  })

  it('input description', () => {
    cy.get('#description').type('description2');
    cy.get('#description').should('have.value', 'description2');
  })

  it('click button', () => {
    cy.get('#btn').click()
  })

  it('click button', () => {
    cy.get('#add-btn').click()
  })

  it('input title', () => {
    cy.get('#title').type('title3');
    cy.get('#title').should('have.value', 'title3');
  })

  it('input description', () => {
    cy.get('#description').type('description3');
    cy.get('#description').should('have.value', 'description3');
  })

  it('click button', () => {
    cy.get('#btn').click()
  })

  it('click button', () => {
    cy.get('#add-btn').click()
  })

  it('input title', () => {
    cy.get('#title').type('title4');
    cy.get('#title').should('have.value', 'title4');
  })

  it('input description', () => {
    cy.get('#description').type('description4');
    cy.get('#description').should('have.value', 'description4');
  })

  it('click button', () => {
    cy.get('#btn').click()
  })

  it('visit http://localhost:4200/tutorials', () => {
    cy.visit('http://localhost:4200/tutorials')
  })

  it('check have less 4 item', () => {
    cy.get('#index0')
    cy.get('#index1')
    cy.get('#index2')
    cy.get('#index3')
  })

})