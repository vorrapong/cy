import { defineConfig } from "cypress";

export default defineConfig({
  videoCompression: 32,
  video: true,
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
});
